<?php

declare(strict_types=1);

use SimpleSAML\Module;

$this->data['header'] = $this->t('{privacyidea:privacyidea:header}');

$this->data['head'] .= '<link rel="stylesheet" href="'
    . htmlspecialchars(Module::getModuleUrl('privacyidea/css/loginform.css'), ENT_QUOTES)
    . '" media="screen" />';

$this->includeAtTemplateBase('includes/header.php');

// Prepare error case to show it in UI if needed
if ($this->data['errorCode'] !== null) {
    ?>

    <div class="error-dialog">
        <img src="/<?php echo htmlspecialchars($this->data['baseurlpath'], ENT_QUOTES);
        ?>resources/icons/experience/gtk-dialog-error.48x48.png"
             class="float-l erroricon" alt="gtk-dialog-error"/>
        <h2><?php echo $this->t('{login:error_header}'); ?></h2>
        <p>
            <strong>
    <?php
    echo htmlspecialchars(
        sprintf(
            '%s%s: %s',
            $this->t(
                '{privacyidea:privacyidea:error}'
            ),
            $this->data['errorCode'] ? (' ' . $this->data['errorCode']) : '',
            $this->data['errorMessage']
        )
    ); ?>
            </strong>
        </p>
    </div>

    <?php
}  // end of errorcode
?>

    <div class="container">
        <div class="login">
            <?php
            if ($this->data['authProcFilterScenario']) {
                echo '<h2>' . htmlspecialchars($this->t('{privacyidea:privacyidea:login_title_challenge}')) . '</h2>';
            } elseif ($this->data['step'] < 2) {
                echo '<h2>' . htmlspecialchars($this->t('{privacyidea:privacyidea:login_title}')) . '</h2>';
            }
            ?>

            <form action="FormReceiver.php" method="POST" id="piLoginForm" name="piLoginForm" class="loginForm">
                <div class="form-panel first valid" id="gaia_firstform">
                    <div class="slide-out ">
                        <div class="input-wrapper focused">
                            <div class="identifier-shown">
                                <?php
                                if ($this->data['forceUsername']) {
                                    if (!empty($this->data['username'])) {
                                        ?>
                                        <h3><?php echo htmlspecialchars($this->data['username']); ?></h3>
                                        <?php
                                    } ?>
                                    <input type="hidden" id="username" name="username"
                                           value="<?php
                                            echo htmlspecialchars($this->data['username'] ?? '', ENT_QUOTES); ?>"/>
                                    <?php
                                } elseif ($this->data['step'] <= 1) {
                                    ?>
                                    <p>
                                        <label for="username" class="sr-only">
                                            <?php echo $this->t('{login:username}'); ?>
                                        </label>
                                        <input type="text" id="username" tabindex="1" name="username" autofocus
                                               value="<?php
                                                echo htmlspecialchars($this->data['username'], ENT_QUOTES); ?>"
                                               placeholder="<?php
                                                echo htmlspecialchars($this->t('{login:username}'), ENT_QUOTES); ?>"
                                        />
                                    </p>
                                    <?php
                                }

                                // Remember username in authproc
                                if (!$this->data['authProcFilterScenario']) {
                                    if ($this->data['rememberUsernameEnabled'] || $this->data['rememberMeEnabled']) {
                                        $rowspan = 1;
                                    } elseif (array_key_exists('organizations', $this->data)) {
                                        $rowspan = 3;
                                    } else {
                                        $rowspan = 2;
                                    }
                                    if ($this->data['rememberUsernameEnabled'] || $this->data['rememberMeEnabled']) {
                                        if ($this->data['rememberUsernameEnabled']) {
                                            echo str_repeat("\t", 4);
                                            echo '<input type="checkbox" id="rememberUsername" tabindex="4"'
                                            . ' name="rememberUsername" value="Yes" ';
                                            echo $this->data['rememberUsernameChecked'] ? 'checked="Yes" /> ' : '/> ';
                                            echo htmlspecialchars($this->t('{login:remember_username}'));
                                        }
                                        if ($this->data['rememberMeEnabled']) {
                                            echo str_repeat("\t", 4);
                                            echo '<input type="checkbox" id="rememberMe" tabindex="4"'
                                            . ' name="rememberMe" value="Yes" ';
                                            echo $this->data['rememberMeChecked'] ? 'checked="Yes" /> ' : '/> ';
                                            echo htmlspecialchars($this->t('{login:remember_me}'));
                                        }
                                    }
                                } ?>

                                <!-- Pass and OTP fields -->
                                <?php if (!$this->data['authProcFilterScenario']) { ?>
                                <label for="password" class="sr-only">
                                    <?php echo $this->t('{privacyidea:privacyidea:password}'); ?>
                                </label>
                                <input id="password" name="password" tabindex="1" type="password" value="" class="text"
                                       placeholder="<?php
                                        echo htmlspecialchars($this->data['passHint'], ENT_QUOTES); ?>"/>
                                <?php } ?>

                                <?php if ($this->data['step'] > 1) { ?>
                                <p id="message" role="alert"><?php
                                $messageOverride = $this->data['messageOverride'] ?? null;
                                if ($messageOverride === null || is_string($messageOverride)) {
                                    echo htmlspecialchars(
                                        $messageOverride ?? $this->data['message'] ?? '',
                                        ENT_QUOTES
                                    );
                                } elseif (is_callable($messageOverride)) {
                                    echo call_user_func($messageOverride, $this->data['message'] ?? '');
                                }
                                ?></p>
                                <?php } ?>

                                <?php if ($this->data['step'] > 1) { ?>
                                <p>
                                    <label for="otp" class="sr-only">
                                        <?php echo $this->t('{privacyidea:privacyidea:otp}'); ?>
                                    </label>
                                    <input id="otp" name="otp" type="password" placeholder="<?php
                                     echo htmlspecialchars($this->data['otpHint'], ENT_QUOTES); ?>"
                                    <?php if (($this->data['otpAvailable'] ?? true) && $this->data['noAlternatives']) {
                                        echo ' autofocus';
                                    } ?>>
                                </p>
                                <?php } ?>

                                <p>
                                    <button id="submitButton" tabindex="1" class="rc-button rc-button-submit"
                                     type="submit" name="Submit" value="1">
                                        <?php echo htmlspecialchars($this->t('{login:login_button}'), ENT_QUOTES); ?>
                                    </button>
                                </p>

                                <!-- Undefined index is suppressed and the default is used for these values -->
                                <input id="mode" type="hidden" name="mode" value="otp"
                                       data-preferred="<?php
                                        echo htmlspecialchars($this->data['mode'], ENT_QUOTES); ?>"/>

                                <input id="pushAvailable" type="hidden" name="pushAvailable"
                                       value="<?php echo ($this->data['pushAvailable'] ?? false) ? 'true' : ''; ?>"/>

                                <input id="otpAvailable" type="hidden" name="otpAvailable"
                                       value="<?php echo ($this->data['otpAvailable'] ?? true) ? 'true' : ''; ?>"/>

                                <input id="webAuthnSignRequest" type="hidden" name="webAuthnSignRequest"
                                       value='<?php
                                        echo htmlspecialchars($this->data['webAuthnSignRequest'] ?? '', ENT_QUOTES);
                                        ?>'/>

                                <input id="u2fSignRequest" type="hidden" name="u2fSignRequest"
                                       value='<?php
                                        echo htmlspecialchars($this->data['u2fSignRequest'] ?? '', ENT_QUOTES); ?>'/>

                                <input id="modeChanged" type="hidden" name="modeChanged" value=""/>
                                <input id="step" type="hidden" name="step"
                                       value="<?php
                                        echo htmlspecialchars(strval(($this->data['step'] ?? null) ?: 2), ENT_QUOTES);
                                        ?>"/>

                                <input id="webAuthnSignResponse" type="hidden" name="webAuthnSignResponse" value=""/>
                                <input id="u2fSignResponse" type="hidden" name="u2fSignResponse" value=""/>
                                <input id="origin" type="hidden" name="origin" value=""/>
                                <input id="loadCounter" type="hidden" name="loadCounter"
                                       value="<?php
                                        echo htmlspecialchars(
                                            strval(($this->data['loadCounter'] ?? null) ?: 1),
                                            ENT_QUOTES
                                        ); ?>"/>

                                <!-- Additional input to persist the message -->
                                <input type="hidden" name="message"
                                       value="<?php
                                           echo htmlspecialchars($this->data['message'] ?? '', ENT_QUOTES); ?>"/>

                                <?php
                                    // If enrollToken load QR Code
                                if (isset($this->data['tokenQR'])) {
                                    echo htmlspecialchars($this->t('{privacyidea:privacyidea:scan_token_qr}')); ?>
                                    <div class="tokenQR">
                                        <?php echo '<img src="' . $this->data['tokenQR'] . '" />'; ?>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>

                            <?php
                            // Organizations
                            if (array_key_exists('organizations', $this->data)) {
                                ?>
                                <div class="identifier-shown">
                                    <label for="organization"><?php
                                    echo htmlspecialchars($this->t('{login:organization}')); ?></label>
                                    <select id="organization" name="organization" tabindex="3">
                                        <?php
                                        if (array_key_exists('selectedOrg', $this->data)) {
                                            $selectedOrg = $this->data['selectedOrg'];
                                        } else {
                                            $selectedOrg = null;
                                        }

                                        foreach ($this->data['organizations'] as $orgId => $orgDesc) {
                                            if (is_array($orgDesc)) {
                                                $orgDesc = $this->t($orgDesc);
                                            }

                                            if ($orgId === $selectedOrg) {
                                                $selected = 'selected="selected" ';
                                            } else {
                                                $selected = '';
                                            }

                                            echo '<option ' . $selected . 'value="' . htmlspecialchars(
                                                $orgId,
                                                ENT_QUOTES
                                            ) . '">' . htmlspecialchars($orgDesc) . '</option>';
                                        } ?>
                                    </select>
                                </div>
                                <?php
                            } ?>
                        </div> <!-- focused -->
                    </div> <!-- slide-out-->
                </div> <!-- form-panel -->

                <?php if (!$this->data['noAlternatives'] && $this->data['step'] > 1) { ?>
                <div id="AlternateLoginOptions" class="groupMargin hidden js-show">
                    <h3><?php echo $this->t('{privacyidea:privacyidea:alternate_login_options}'); ?></h3>
                    <!-- Alternate Login Options-->
                    <?php if (($this->data['webauthnAvailable'] ?? false) && $this->data['mode'] !== 'webauthn') { ?>
                    <button id="useWebAuthnButton" name="useWebAuthnButton" type="button">
                        <span><?php echo $this->t('{privacyidea:privacyidea:webauthn}'); ?></span>
                    </button>
                    <?php } ?>
                    <?php if (($this->data['pushAvailable'] ?? false) && $this->data['mode'] !== 'push') { ?>
                    <button id="usePushButton" name="usePushButton" type="button">
                        <span><?php echo $this->t('{privacyidea:privacyidea:push}'); ?></span>
                    </button>
                    <?php } ?>
                    <?php if (($this->data['otpAvailable'] ?? true) && $this->data['mode'] !== 'otp') { ?>
                    <button id="useOTPButton" name="useOTPButton" type="button">
                        <span><?php echo $this->t('{privacyidea:privacyidea:otp}'); ?></span>
                    </button>
                    <?php } ?>
                    <?php if (($this->data['u2fAvailable'] ?? false) && $this->data['mode'] !== 'u2f') { ?>
                    <button id="useU2FButton" name="useU2FButton" type="button">
                        <span><?php echo $this->t('{privacyidea:privacyidea:u2f}'); ?></span>
                    </button>
                    <?php } ?>
                </div>
                <?php } ?>
            </form>

            <?php
            // Logout
            if (($this->data['showLogout'] ?? true) && isset($this->data['LogoutURL'])) { ?>
                <p>
                    <a href="<?php
                    echo htmlspecialchars($this->data['LogoutURL']); ?>"><?php echo $this->t('{status:logout}'); ?></a>
                </p>
            <?php } ?>
        </div>  <!-- End of login -->
    </div>  <!-- End of container -->

<?php
if (!empty($this->data['links'])) {
    echo '<ul class="links">';
    foreach ($this->data['links'] as $l) {
        echo '<li><a href="' . htmlspecialchars($l['href'], ENT_QUOTES) . '">' . htmlspecialchars(
            $this->t($l['text'])
        ) . '</a></li>';
    }
    echo '</ul>';
}
?>

    <script src="<?php echo htmlspecialchars(Module::getModuleUrl('privacyidea/js/pi-webauthn.js'), ENT_QUOTES); ?>">
    </script>

    <script src="<?php echo htmlspecialchars(Module::getModuleUrl('privacyidea/js/u2f-api.js'), ENT_QUOTES); ?>">
    </script>

    <meta id="privacyidea-step" name="privacyidea-step" content="<?php echo $this->data['step']; ?>">

    <meta id="privacyidea-translations" name="privacyidea-translations" content="<?php
    echo htmlspecialchars(json_encode($this->data['translations'])); ?>">

    <script src="<?php echo htmlspecialchars(Module::getModuleUrl('privacyidea/js/loginform.js'), ENT_QUOTES); ?>">
    </script>

<?php
$this->includeAtTemplateBase('includes/footer.php');
?>
